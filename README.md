To compile with Visual Studio add all required dependencies in the project properties:

- VC++ Directories
    - Executable Directories should contain a path to the <libusb>\bin
    - Include Directories should contain a path to the <libusb>\include and local <testbulk>\src
    - Library Directories should contain a path to the <libusb>\lib\msvc
    
- Linker 
    - Input should contain libusb.lib and winmm.lib
    