#include "geometry.h"
#include <stdio.h>

/// the resolution of the optical sensor in DPI
int sensorDPI_1 = 266;
int sensorDPI_2 = 1105;

/// for length calculations
float inchToMeter = (float)0.0254;

/// Calculates the world coordinates out of the raw sensor data
/// - get the coordinates within the real world in m
/// - convert sensor data from pixel into meters
/// - save current position, if it is different than the previous position
/// - parameters
///		- x1, y1, x2, y2: the sensor x and y data
struct fCoord math_getWorldCoord(int x1, int y1, int x2, int y2, float gain)
{
    struct fCoord a;

    // for calibration: 
    // pixel_x+= x1;
    // pixel_y+= x2;
    // printf("x1= %f     x2= %f\n", pixel_x, pixel_y);

    a.x = fPosition_old.x + x1*(inchToMeter / sensorDPI_1)*gain;
    a.y = fPosition_old.y - x2*(inchToMeter / sensorDPI_2)*gain;
    a.z = (float)0.1; // always near to zero (z points upward);

    fPosition_old.x = a.x;
    fPosition_old.y = a.y;

    a.rotX = 0.0;			// pitch doesn't depend on the ball movement
    a.rotY = 0.0;			// neither does roll
    a.rotZ = 0.0;			// the animal turns around, not the environement
    return(a);
}

/// Sets the position to a value given by Vizard
/// parameters:
///		- vizard coordinates as struct p3
void math_beam(struct p3 p)
{
    fPosition_old.x = p.x;
    fPosition_old.y = p.y;
}
