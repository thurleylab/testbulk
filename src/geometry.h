//geometry.h
#ifndef GEOMETRY_H
#define GEOMETRY_H 
//----------------------------------------------------------------------------------------------------------------------------------------
/// change sensorDPI if you change the sensor or recalibrate
extern int sensorDPI; 		/// sensor resolution -> check mouse datasheet
extern float inchToMeter; 	/// conversion inch to meter
#define PI 3.14159

float pixel_x, pixel_y;

/// Point structure for x and y coordinates+
struct iPoint {
    int x;
    int y;
};

struct fPoint {
    float x;
    float y;
};

struct p3
{
    float x;
    float y;
    float z;
    float gain;
};

struct fPoint fPosition_old; /// previous position within real world

struct MouseData
{
    char devicePath[200];	/// individual device address
    int x;					/// x in pixel
    int y;					/// y in pixel
};
struct MouseData mouse[2];	/// sensor data

struct fCoord				/// our data: translation and rotation
{
    float x;
    float y;
    float z;
    float rotX;
    float rotY;
    float rotZ;
};
struct fCoord worldCoord_m;	/// current position in m
struct fCoord math_getWorldCoord(int x1, int y1, int x2, int y2, float gain);
void math_beam(struct p3 p);

#endif
