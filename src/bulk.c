#include <windows.h>
#include <lusb0_usb.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <geometry.h>
/// the devices vendor and product id 
#define SENSOR_WEST_VID 0x046d
#define SENSOR_WEST_PID 0xc051
#define SENSOR_NORTH_VID 0x046d
#define SENSOR_NORTH_PID 0xc24c

/// the devices endpoints 
#define EP_IN 0x81
#define EP_OUT 0x03

#define BUF_SIZE 8 /// size of raw mouse data
#define MAP_SIZE 49 /// total length of the memory mapped filein byte

#define DEBUG 

//-------------------------------
/// coordinate FILE
FILE* cstream = NULL;
const char cfile[]="RawCoordinatesToMmap.txt";

//----------------------------------------------------------------------------------------------------------------------------------------
/// Timer
int uDelay;
HANDLE hTimer = NULL;
TIMECAPS tc;
UINT timerID;

//---------------------------------------------------------------------------------
// LibUSB data
usb_dev_handle *hDevice[2] = {NULL, NULL}; /// array for the two device handles
char tmp[BUF_SIZE]; /// data buffer
LPCTSTR USBfileName[2]; /// string identifier of the usb devices
int dev_index; /// tells which mouse is next to be read

//----------------------------------------------------------------------------------------------------------------------------------------
// File Mapping
/// name of the shared file
TCHAR szName[]=TEXT("Global\\MyFileMappingObject"); 
/// 28 bytes to save our data (7 float values). Should be identical in the listener program
TCHAR szMsg[]=TEXT("012345678901234567890123456");		
/// handle of the mapped file
HANDLE hMapFile;
/// buffer used to copy data into the shared memory space
LPCTSTR pBuf; // LPCTSTR= const char *

struct p3 vizard_coord;
float globgain;
UINT ti;

/// Sets the size of the current command shell
/// useful if you work with command line progams
/// and you need longer lines for data output
/// input:
///		- int x : x size
///		- int y : y size
/// output: void
void SetWindowExtensions(int x, int y)
{
	SMALL_RECT sr_window;
	COORD      extension;

	sr_window.Left    = 0;
	sr_window.Top     = 0;
	sr_window.Right   = x - 1;
	sr_window.Bottom  = y - 1;

	extension.X = x;
	extension.Y = y;

	SetConsoleWindowInfo (GetStdHandle(STD_OUTPUT_HANDLE), TRUE, &sr_window);
	SetConsoleScreenBufferSize (GetStdHandle(STD_OUTPUT_HANDLE), extension);
}

//-----------------------------------------------------------------------------------------
// Memory mapping

/// Initializes the memory mapping file
/// we will write the world coordinates into the shared memory
/// in the function "memMap_write()"
/// inputs: none
/// output: void
int mmap_createFile()
{
	hMapFile = CreateFileMapping(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security 
		PAGE_READWRITE,          // read/write access
		0,                       // max. object size 
        MAP_SIZE,                // buffer size  
		szName);                 // name of mapping object
	if (hMapFile == NULL) 
	{ 
		printf("CreateFileMapping failed, errorno. %i", GetLastError());
		return -1;
	}
	pBuf = (LPTSTR) MapViewOfFile(hMapFile,   // handle to map object
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,                   
		0,                   
        MAP_SIZE);
	if (pBuf == NULL) 
	{ 
		CloseHandle(hMapFile);
		printf("MapViewOfFile failed, errorno. %d", GetLastError());
		return -1;
	}
	printf("Memory mapped file has been created \n");
	return(0);
}

/// Writes the coordinate data (global var worldCoord_m) into
/// the shared memory
/// input: none
/// output: void
void memMap_write()
{	
	float *pFloat;		// pointer to float, we will extract floats from a longer array
	float *pMsg;		// pointer to the shared memory buffer


	pMsg= (float*)&szMsg[0]; 		// set the target pointer to the first sign of szMsg
	pFloat= &worldCoord_m.x;		// set source pointer
	memcpy(pMsg, pFloat, sizeof(float)); // copy float value into target
	pMsg++;	
	pFloat= &worldCoord_m.y;
	memcpy(pMsg, pFloat, sizeof(float));
	pMsg++;
	pFloat= &worldCoord_m.z;
	memcpy(pMsg, pFloat, sizeof(float));
	pMsg++;
	pFloat= &worldCoord_m.rotX;
	memcpy(pMsg, pFloat, sizeof(float));
	pMsg++;
	pFloat= &worldCoord_m.rotY;
	memcpy(pMsg, pFloat, sizeof(float));
	pMsg++;
	pFloat= &worldCoord_m.rotZ;
	memcpy(pMsg, pFloat, sizeof(float));
	pMsg++;
	pFloat= (float*)(&ti);
	memcpy(pMsg, pFloat, sizeof(float));

	if(ti%11==0){
		fprintf(cstream, "%u %f %f\n", ti, worldCoord_m.x, worldCoord_m.y);
	}
	CopyMemory((PVOID)pBuf, szMsg, sizeof(worldCoord_m));
}

/// Reads the update flag from Vizard
/// input: none
/// output: void
int memMap_update()
{
    if (pBuf[28] != 0)
        return(TRUE);
    else {
        return(FALSE);
    }
}

/// Resets the update flag 
/// input: none
/// output: void
void memMap_resetUpdate()
{
	CHAR* b;
	b= (CHAR*)&pBuf[28];
	*b=0;
}
/// Reads the coordinates from the memory mapped file
/// input: none
/// output: void
struct p3 memMap_getVizardCoord()
{
	struct p3 p;
	float *pFloat;		
	pFloat= (float*)&pBuf[29];
	p.x= *pFloat;
	pFloat++;
	p.y= *pFloat;
	pFloat++;
	p.z= *pFloat;
	pFloat++;
	p.gain=*pFloat;

	//fprintf(stderr,"%f", p.gain);

	return(p);
}

//-------------------------------------------------------------------------------------
// USBlib

/// Checks wether a specific device has already been opened
/// in case we have several devices with identical HID and VID
/// input:
///	- char *name: usb_device.descriptor.idProduct, string containing the device name 
/// (including HID and VID)
/// 	- output:
///	- int: 1 if device already registered, otherwise 0
int alreadyOpen(char *name)
{
	int i;
	for(i=0; i<=1; i++)
	{
		if (USBfileName[i] == (LPCTSTR)name)
			return(1);
	}
	return(0);
}

/// Opens a USB device with the specified HID and VID
/// input:
///	- int iVID, int iPID: vendor and device ID 
///	- int iDevNumber: global counter, used to save the recognised devices
/// output:
///	- usb_dev_handle*: device handle or NULL if opening fails
usb_dev_handle *mouse_openDev(int iVID, int iPID, int iDevNumber)
{
	struct usb_bus *bus;
	struct usb_device *dev;
	char description[256];
	// check all usb busses
	for(bus = usb_get_busses(); bus; bus = bus->next) 
	{
		// printf("On USB bus: %s \n", bus->dirname);
		// check all devices on a single bus
		for(dev = bus->devices; dev; dev = dev->next) 
		{
			printf("   device %s \n", dev->filename);
			//printf("DEBUG:   vendorid %04x \n", dev->descriptor.idVendor);
			//printf("DEBUG:   productid %04x \n", dev->descriptor.idProduct);
			// if the desired VID & PID foud, open the device
			if(dev->descriptor.idVendor == iVID
				&& (dev->descriptor.idProduct == iPID) 
				&& alreadyOpen(dev->filename) == 0)
			{				
				printf("---> sensor found: VENDORID = %04x, PRODUCTID = %04x\n", dev->descriptor.idVendor, dev->descriptor.idProduct);
				printf(description + strlen(description), sizeof(description) -
					strlen(description), "%04X", dev->descriptor.idProduct);
				USBfileName[iDevNumber]= (LPCTSTR)dev->filename;
				return usb_open(dev);
			}
		}
	}
	return NULL; // couldn`t find the device, return NULL
}

/// Releases a specific USB device
/// input
///		- usb_dev_handle* dev: pointer to the USB device handle
/// output:
///		void
void mouse_release(usb_dev_handle* dev)
{
	usb_release_interface(dev, 0);
	usb_close(dev);
}

/// release all USB device handles
/// input: none
/// output: void
void mouse_releaseAll()
{
	if(hDevice[0] != NULL)
		mouse_release(hDevice[0]);
	if (hDevice[1] != NULL)
		mouse_release(hDevice[1]);
}

/// Opens a specific USB device and gets the handle
/// Then it writes the handle into the global var USBfileName[]
/// input:
///		- int iVID, int iPID: HID and VID of the USB device
///		- int iDevNumber: device number
/// output:
///		- usb_dev_handle* : pointer to the device handle
usb_dev_handle *mouse_getHandle(int iVID, int iPID, int iDevNumber)
{
	usb_dev_handle *dev = NULL; // the device handle
	if(!(dev = mouse_openDev(iVID, iPID, iDevNumber)))
	{
		printf("error: target device not found \n");
		return NULL;
	}
	if(usb_set_configuration(dev, 1) < 0)
	{
		printf("error: usb_set_configuration(dev, 1) failed\n");
		usb_close(dev);
		return NULL;
	} 
	if(usb_claim_interface(dev, 0) < 0)
	{
		printf("error: claiming interface 0 failed\n");
		usb_close(dev);
		return NULL;
	} 
	if(usb_clear_halt(dev, 0x81))
	{
		printf("error: cleat halt failed\n");
		usb_close(dev);
		return NULL;
	} 
	return(dev);
}

/// Reads the x and y values from a Logitech mx518 mouse data frame
/// the mouse returns a 8 byte message: x= bytes 4..5 and y= bytes 7..8
/// input:
///	-char p[]: mouse message
/// output:
///	- struct fPoint: x and y coordinates as fPoint
struct iPoint getXY_mx518(char ch[])
{
	struct iPoint p = {0, 0};
	p.x= (int)(ch[5]<<8 | (unsigned char)ch[4]);
	p.y= (int)(ch[7]<<8 | (unsigned char)ch[6]);
	return(p);
}

/// Reads 8 bytes from the specified device
/// - parameters:
///		- dev (in): pointer to the device handle
///		- tmp (out): pointer to the data buffer
/// If you encounter problems with reading from the device, you can try the interrupt too:
///		readSize=usb_interrupt_read(dev, EP_IN, tmp, 8, 6);
BOOL mouse_get8bytes(usb_dev_handle* dev, char* tmp)
{
	int readSize=0; // number of bytes to read
	// get data from the device
	readSize=usb_bulk_read(dev, EP_IN, tmp, 8, 10);
	// if no data available, then set all fields to zero
	if(readSize < 0)
	{
		// printf("--- Error %d in mouse_get8bytes  \n", readSize);
		tmp[0]=0x00;
		tmp[1]=0x00;
		tmp[2]=0x00;
		tmp[3]=0x00;
		tmp[4]=0x00;
		tmp[5]=0x00;
		tmp[6]=0x00;
		tmp[7]=0x00;
		return(FALSE);
	} 
	return(TRUE);
}

/// Multimedia Timer Interrupt
void CALLBACK TimeProc(UINT wTimerID, UINT msg,
						DWORD dwUser, DWORD dw1, DWORD dw2) 
{
	struct p3 vp;
	struct iPoint p;

	// check if Vizard resets the coordinates
	if(memMap_update())
	{
		// reset the flag
		memMap_resetUpdate();
		vp= memMap_getVizardCoord();
		math_beam(vp);
		globgain=vp.gain;
		// printf("update vizard coordinates: %f %f %f\n", vp.x, vp.y, vp.z);
	}

	if(hDevice[dev_index] != NULL)
	{
		// read raw data from the current mouse
		mouse_get8bytes(hDevice[dev_index], &tmp[0]); 
		// printf("%i: %08X %08X %08X %08X \n", dev_index, tmp[4], tmp[5], tmp[6], tmp[7]);
		// convert raw data into integers
		p= getXY_mx518(tmp); 
		mouse[dev_index].x= p.x; // get x
		mouse[dev_index].y= p.y; // get y
	}

	if(dev_index == 1)
	{
		worldCoord_m= math_getWorldCoord(mouse[0].x, mouse[0].y, 
						 mouse[1].x, mouse[1].y, globgain); 
		memMap_write();
	}

    // flip the devioce index
	dev_index= 1- dev_index;
	
	//update time index
	ti++;
}


/// Sets the timer interval
/// parameters:
///	- interval in ms
/// output: 1 if no error occurs, otherwise 0
UINT SetTimerCallback(UINT msInterval)                // event interval
{ 
	timerID = timeSetEvent(
		msInterval,                  // delay
		(UINT)0,                     // resolution (global variable)
		//(LPTIMECALLBACK)TimeProc,  // callback function
        TimeProc,
		NULL,                       // user data
		(UINT)TIME_PERIODIC );      //  timer event

	if(!timerID)
	{
		printf("Timer error\n");
		return 0; //ERR_TIMER;
	}
	else
	{
		printf("Timer OK, interval between single data points= %i ms\n", uDelay);
		return 1; //ERR_NOERROR;
	}
}

/// Destroys the timer object
/// input: none
/// output: void
void DestroyTimer()
{
	if(timerID) {                // is timer event pending?
		timeKillEvent(timerID);  // cancel the event
		timerID = 0;
	}
} 
/// main
int main(int argc, char** argv)
{
	int freq=40;
	int device=0;
	int ready= FALSE;
	int io_OK = FALSE;

    fPosition_old.x= 0; // initiate position x
	fPosition_old.y= 0; // initiate position y
	dev_index=0; // start reading at optical device 0
	globgain=1.;
	ti=0;

	cstream=fopen(cfile,"w");
	if(!cstream){
	  printf("Error opening %s!\n", cfile);
	  printf("Exiting...\n");
	  exit(1);
	}

	SetWindowExtensions(150, 20);
	printf("\n--------------\nLibUSB Testprogram\n--------------\n\n");
	if (argc == 2) {
        freq = atoi(argv[1]);
	}
	else {
        printf("Usage: testbulk [frequency in Hz] \n");
        printf("Example for frequency of 40 Hz:\n");
        printf("	testbulk 40\n");
	}

    uDelay = ceil(((float)1 / (float)(freq * 2)) * 1000);
    ready = TRUE;
    
    usb_init(); /* initialize the library */
	usb_find_busses(); /* find all busses */
	usb_find_devices(); /* find all connected devices */

    hDevice[0] = mouse_getHandle(SENSOR_WEST_VID, SENSOR_WEST_PID, 0);
    hDevice[1] = mouse_getHandle(SENSOR_NORTH_VID, SENSOR_NORTH_PID, 1);

	mmap_createFile();	// create memory mapped file

	// start timer
	if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) == TIMERR_NOERROR) 
		ready= TRUE;
	else
		printf("ERROR: timer not started\n");
	// set the timer interval and start the timer
	if (ready)
	{
		timeBeginPeriod(uDelay); 
		SetTimerCallback(uDelay);
	}
	// wait for key press to close the program
	do{
	} while (_kbhit() == 0);

	// release timer
	DestroyTimer();
	// release all initialized mice
	mouse_releaseAll();
	// close mapped file
	UnmapViewOfFile(pBuf);
	// free ressources
	CloseHandle(hMapFile);

	if(cstream) fclose(cstream);

	return 0;
}
