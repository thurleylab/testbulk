import numpy as np


# ball dimensions
r = 0.25 # meters
meridian = 2 * np.pi * r

# Values for sensors used while measurement:
sensorDPI_1 = 500   # taken from testbulk
sensorDPI_2 = 1000  # taken from testbulk

# X
px1 = [0.763157308101654, 0.10000000149011612, 0.7499744892120361]
px2 = [1.5982589721679688, 0.10000000149011612, 0.7321445941925049]
px3 = [2.4355452060699463, 0.10000000149011612, 0.7165994644165039]
# wrong measured?: [2.479332208633423, 0.10000000149011612, 0.7152025699615479]
px4 = [3.272372007369995, 0.10000000149011612, 0.7017918825149536]
px5 = [4.10493278503418, 0.10000000149011612, 0.6869579553604126]

# Y
py1 = [0.7560453414916992, 0.10000000149011612, -0.9342770576477051]
py2 = [0.8104524612426758, 0.10000000149011612, -2.6686344146728516]
py3 = [0.8652154207229614, 0.10000000149011612, -4.4142537117004395]
py4 = [0.9175901412963867, 0.10000000149011612, -6.148855209350586]
py5 = [0.9678819179534912, 0.10000000149011612, -7.877603054046631]


dx = np.array([px1[0], px2[0], px3[0], px4[0], px5[0]])
dy = np.array([py1[2], py2[2], py3[2], py4[2], py5[2]])

# actual measured distance in VR along meridian
avg_x = np.abs(np.average(np.diff(dx)))
avg_y = np.abs(np.average(np.diff(dy)))

# correcting coefficients
ccx = meridian / avg_x
ccy = meridian / avg_y

# correct sensor values
sensorDPI_x = sensorDPI_1 / ccx 
sensorDPI_y = sensorDPI_2 / ccy

# for NEW vrCinema software:
inchToMeter = 0.0254  # taken from testbulk
speed_x = inchToMeter / sensorDPI_x
speed_y = inchToMeter / sensorDPI_y


print(avg_x, avg_y)
print(ccx, ccy)
print(sensorDPI_x, sensorDPI_y)
print(speed_x, speed_y)